# Checklist for participants - Users

**Date and Time**: Your interview with the Tor Project is scheduled for [date + time in local timezone]. The session will take between [estimated duration] minutes.
 
**How to join**: Our room is private and only shared with you and researchers.
Link: [room link]
Password (if needed): [pass]

**For the session, you will need**: 

- Internet connection. If you have a VPN activated, please turn it OFF for this test.
- Your device ready for screen sharing (e.g., if you want to use your phone, ensure that we can see your screen during the session).
- We'd love to see you in your cam, but that is up to you. If you are up for sharing your camera, please check the local permissions before the session.

**Consent form**: Please, sign this consent form before the session and return it to us. If you have any questions, just let us know.
[Link to consent form]

Thank you for participating in this user research!

The Tor UX team
