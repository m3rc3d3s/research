## Background

The purpose of this study is to test the flow and information structure of our discourse forum. If users are not able to find what they are looking for in the proposed structure it can result in frustration for them, causing a bad experience. We will be using the **Tree Testing** [methodology](https://www.nngroup.com/articles/tree-testing/) to understand the user experience with the flow, and generating a quantitative analysis report.

## Objectives

Evaluate the current structure for the discourse forum, our user flow, and any immediate pain point users may face when trying to find a topic or post a new entry in the forum.

## Participants

For this study, we will recruit participants who are already part of the Tor Community or some communication channel, such as mailing lists, that indicates their familiarity with collaborating or testing Tor products.

We expect between 15-50 participants for this study.

### Recruitment Strategy

We will send a Call For Participation (CFP) to: global-south, tor-qa, tor-project, community mailing lists.

## Methodology

In this Research Plan we will be using **Tree Testing** Methodology to ask research participants to complete findability tasks by using an information structure presented to them. This method will enable us to understand our users flow, ensuring we are using the right (or not) structure for the forum.

We will use a self-hosted LimeSurvey to create the activity for this study. This activity shouldn't last more than 10-15 minutes.

### We will need:
1. Instructions
2. Questions
3. Tasks

#### Instructions: 
- You will be asked to find a certain category on a list that resonate to your need;
- Click through the list until you get to the one that you think helps you complete the task;
- If you don't find the category you're looking for, you can let us know in a follow-up question.


Warning: Please don’t use the back button on your browser. This is not a test of your ability, there are no right or wrong answers.

#### Questions

Do users can successfully post a new topic using our discourse structure?
Do people feel more confident giving us feedback through discourse?
How many people successfully report a problem and search for at least one update of our product using discourse?

#### Tasks

Our research participants are familiar with one or more Tor products, and are potential participants for the forum. We will give them hypothetical 'scenarios' based on their uses of Tor products. We should use different language than the labels in the forum structure, and we only need one or two sentences for the tasks. We aim to provide 8 tasks in total.

1. Imagine you were navigating through Tor Browser Android and it crashed after trying to load your e-mail page. Use the tree below to indicate where you can share this problem.
2. A recent Tor Browser update has changed the way an existing feature works, and you don't like it. Where would you voice your opinion about the change?

3. An onion site you're trying to visit is not loading. Where would you look for help?

4. When navigating on The Tor Project website, you found a translation mistake and you want to report it. Where you go to do it?

5. A website you're usually to visit is not working. Where would you go to ask for help?

6. You're running a relay, but your relay is very slow. Where would you look for help?

7. You live in a country that is censoring Tor Network and you need to setup a bridge. The bridges provided are not working, where you go to ask for help?

##### Follow-up question for each task

Let's use two Likert Scale questions to measure participants perception about our tasks and structure. 

1. Overall, how confident are you that you've completed the previous task successfully?

2. Overall, how difficult or easy was the task to complete?

#### Tree

|  |  |  |  |
| ------ | ------ | ------ | ------ |
| Forum Rules and Guidelines |  |  |  |
| Announcements |  |  |  |
| Feedback |  |  |  |
| Feedback | Tor Browser Alpha |  |  |
| Feedback | UI/UX |  |  |
| Feedback | Localization |  |  |
| Feedback | Websites |  |  |
| Feedback | Feature request |  |  |
| Support |  |  |  |
| Support | Tor Browser for Android |  |  |
| Support | Tor Browser Desktop |  |  |
| Support | Censorship Circumvention |  |  |
| Support | Onion Services |  |  |
| Support | Relay Operator |  |  |
| Support | Core Tor |  |  |
| In your language |  |  |  |
| In your language | Spanish |  |  |
| In your language | Portuguese |  |  |
| Mailing List|  |  |  |
| General Discussion|  |  |  |


#### Results
We will measure the **Sucess Rate**: the percentage of participants completing the task correctly, and determine if the proposed structure works. We can't measure Speed or Directness rate using LimeSurvey.

### Timeline
We will run this study for one-week, in August 2021.

### Issues
- https://gitlab.torproject.org/tpo/community/support/-/issues/40026
- https://gitlab.torproject.org/tpo/community/support/-/issues/40027


