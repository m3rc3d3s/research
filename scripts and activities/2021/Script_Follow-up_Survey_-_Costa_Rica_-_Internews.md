**Encuesta de seguimiento - Tor Browser - CAF**

¡Hola! Gracias por querer ser parte de este proceso investigativo. Ha pasado un mes y por ende les envío esta encuesta para saber cómo ha sido el uso que han dado a Tor Browser en estas semanas. Contestar esta encuesta no debería tomarle mucho tiempo y nos ayudará en dos cosas: 

1.entender el uso que le dió al navegador en este mes
2.saber cómo desearía ser contactade para una pequeña entrevista

**Q1.** Nombre

**Q2.** ¿Cuáles navegadores ha usado en este mes? -en la computadora-
(Puede seleccionar varios)

- Brave
- Chrome
- Chromium
- Firefox
- Opera
- Safari
- Tor Browser
- Vivaldi

**Q3.** ¿Con qué frecuencia ha usado Tor Browser?
(en este mes)

- Siempre
- Muchas veces
- De vez en cuando
- Pocas veces
- Nunca

**Q4.** En escala del 0 al 10, ¿recomendaría usar Tor Browser a un amigue o familiar? 
(0 siendo nada probable, 10 siendo muy probable)

**Q5**. ¿Por qué? 

**Q6.** ¿Qué medio le gustaría ser contactade? 
(Para poder tener una llamada y ahondar en estas respuestas... )

- Llamada celular
- Videollamada (Jitsi)

**Q7.** ¿Qué horario le convendría? 
(Esta llamada no duraría más de 1h )

- Mañana 
- Tarde
- Noche