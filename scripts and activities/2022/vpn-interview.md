## Tor VPN Interviews
* Location:
* Date:
* Persona:

Goal: Learn about users’ preferences for features and understand their needs when using a VPN – especially concerning their privacy, security and anonymity.

Hello! Thank you for taking this time to talk to us. At this moment, we are researching possible features for a Tor-powered VPN. It will help us to understand your needs when using a VPN. 

**Consent information / data policy:**

We are going to record this interview as part of our research process. We will transcribe and anonymize all responses. All audio records will be deleted within 90 days of their recording.

We also wish to ask for permission to use anonymous excerpts from your response in our public reports. If you give us permission, we may quote part of your response in a public report and/or other publications, such as social media, grant applications, etc. We will not disclose your identity and all quotes will be anonymized. It is possible that we do not use any quotes, even if you give us permission to do so. 

**Do you consent?** Yes / No

Ok, shall we start?


**Warm-up question**

**1. Tell me about when you typically use the internet?** Do you use a VPN? In other words, do you unlock your device and start using it or do you customize your connection? (If user starts their day with their mobile, ask if they use a VPN on it. Users usually explain which device they use and when, but we can ask if they don't mention.)

_If needed, also follow-up with these questions_: And do you usually use it on your computer or cell phone? Do you start the day using a VPN?


### Tor questions

**2. When do you use Tor Browser? Why?** 
Goal: deep on the use of Tor, validate if they are a Tor power user or not.


**3. Tell me about the last time you used Tor.** 
Goal: explore users use of Tor and prepare to ask more Tor specific questions.


 

**4. What’s the feature you hope Tor never gets rid off?** 
Goal: explore their empathy to Tor features.


**5. Have you ever used the New Identity button in Tor Browser?** (visual stimuli) If not, why? How did you use it? What do you expected from it? (new identity feature)


**6. Have you ever used the New Circuit button in Tor Browser?** (visual stimuli) What for? (new circuit feature) 
Goal: understand if users try New Circuit to get specific exit nodes




**7. Have you ever visited an Onion Site before?** If yes, how? Do you keep bookmarks of your onion sites? If no, why?
Goal: Investigate reasons for potentially blocking an onion location




### VPN questions


**8. When do you use a VPN? Why?** (Usually, users will say which device they use with the VPN, if they don't say it, we should ask)
Goal: Deep on the use of VPN, for what and why.
_If needed, also ask:_ Which VPN do you use?



**9. How do you initiate a connection to your VPN?**  (customize connection)




**10. Did you ever need to restart your VPN? Why? OR For what reasons would you restart your VPN app?** 
Goal: Help inform new identity from a VPN-user's perspective


**11. What VPN do you use? Do you pay for it? Why?** 
Goal: Analyze VPN brands.


_If needed, also ask:_ 11.a. Do you have more than one VPN installed? Which one? 11.b. Do you sometimes use tor(-browser) and a VPN at the same time?




**12. Do you customize your VPN's settings? If so, what do you typically change?** (security settings) 
Goal: Explore the VPN usability from the user point of view.




**13. Have you ever used a VPN to change your connection on a specific app? How?** (per-app permissions)
Goal: Find if the user knows/wants this feature.






**14. When your VPN is on, do you avoid using some specific apps?**  (per-app permissions) (If yes, why and which kind of apps - i.e. bank, social media, etc)




**15. Is there any feature that you want and your current VPN doesn't provide?** 




**16. Do you ever select a country manually or leave it automatically on own your VPN?** (exit node)




**17. For what reasons would you switch to a different server or location in your VPN app?** (exit node)
Goal: Help inform new-circuit and new-identity from a VPN-user's perspective




**18. From 1 to 5, how important is selecting a country in a VPN? Consider 1 'not important' and 5 'very important'.** (exit node)




### Security and Privacy questions

Before jumping to Security and Privacy, explain about Tor's mission to warm up about privacy and security.

**19. When you think about Tor, and Tor's mission, what security feature would you expect in a Tor-powered VPN?**
Goal: Explore user’s understanding of security features Tor has. 


**20. What’s your main security concerns when you’re using a VPN?**
Goal: Explore user’s understanding of security issues on the use of a VPN.



**21. How do you expect the VPN to behave in case you lose your VPN connection?**  (killswitch feature) 
*** (if the user can't think about anything, give options: 1. block all internet connection; 2. keeps me online anyway; 3. warn me about it while keeping me connected) 


**22. How would you evaluate if an app could compromise your privacy?** 



**23. How would you evaluate if an app could compromise your privacy even though you're routing it through Tor?**



**24. What would you expect from a Tor-powered VPN in counterpoint to a regular VPN?**




### Final questions

**What three words do you associate with the term VPN?** 
Goal: Find users affinity/empathy to the term VPN.


**What influenced you to pick the VPN you use?** 
Goal: Find users preferences in terms of brand, visual style, SEO, trust network.


**What are the top 3 features you look for in a VPN?**



**What three words do you associate with the term Tor?** 
Goal: Find users affinity/empathy to the term Tor.

