## User Research Community

In this page you can find instructional material, documentation and other files we have in the [Tor Community Portal](https://community.torproject.org/user-research/). You can suggest changes and modifications to this files.


---

Last update: 2021
