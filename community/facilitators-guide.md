# Facilitators Guide

Congrats! You're going to run your first Tor training - and we are very happy to have you onboard!
In this place you will find helpful guidelines that are designed to make your training easier for you and your audience.

First of all, make sure you read the [**Tor Code of Conduct**](http://link-to-code-of-conduct).

Some additional recommendations are:
* Be as inclusive as possible
* Keep up to date[^1]
* Be aware about the Internet laws in your country, as well as [verify if Tor is blocked there](https://link-to-it)[^2].

[^1]: Make sure you have the latest Tor version of the software you're going to present (Tor Browser for Desktop or Android, for example).
[^2]: If Tor is blocked in your country, you must be ready to setup a bridge in your browser, as well as help your audience to do it.

### Coordinate with the user researcher and the Tor community

We don't expect for you to do all this work alone - we will support you with meetings, guides and mentorship. You can join us any time on our IRC channel #tor-community and our [mailing list](https://mailin-list).

It will be great if you have someone to run the [user researcher](https://link-community-ur) in your training. If you don't, let us know and we might find someone to do it there in our community.

You and the user researcher should be aligned about the agenda and time to run interviews (if that is the case) during the end of the training. Also, we want you two to coordinate feedbacks together. Both for you and for your audience.

## Logistics for planning a training
Verify with your community if they have an accessible space for the training.
If they don't, look for community centers or public spaces. But be aware that you're running a training for Human Rights Defenders, so make sure you will have a space that makes possible to keep the privacy of your training.

### Agenda
Make a script for the training, it will be very useful for you to follow the topics you want to share, and make sure your hitting all the points that you planned.
We have a suggestion for each kind of training here:
* [Tor Browser for Desktop](http://link)
* [Tor Browser for Android](http://link)
* [Onion Services](http://link)
* [Tails](http://link)

Please share with us your schedule for the training.

### Communicate to engage!
Tell participants in advance what they should bring to the training. If they want to install Tor Browser for Desktop, it's important to bring their computer along. If they're going to install Tor Browser for Android, make sure they know **it is needed at least 100MB free in their device**, otherwise they won't be able to install it (and it can be very frustrating).

To install Tor Browser, they need to update their time and date in their devices.

### Get prepared!

Print and bring the [Checklist for Trainers](http://link) with you! You can also use it on your computer if you prefer.

Keep in mind that most people have never used Tor before, nor they know how it works. It can be complex, so let's keep it simple. You can download our slides for training [here](http://link).

To **keep it simple, teach the basics**. It's better to understand how the Internet works, than to start with zero days or baseband exploits.

Be a good listener. You're audience maybe shy to ask, so make sure you're giving them time to process the information, and ask their questions. If necessary, ask them from time to time if they have questions (we have some suggested moments to do so on our agenda).

When running your training, make sure you're following the steps listed in our checklist.

### Feedback
Before ending your training, hand out post-its for your audience, you can give one post-it on each color and ask them to fill it with what they think about: 1. the service they just learned; 2. Tor project; and 3. Tor in general. It can also be questions - keep in mind that every feedback is a good feedback.

It is very important for us to hear back from you. We know that when it comes about training, practice makes it better. We want to know how the training was for you, how we can improve our support to engange and build your community and also, if you want to keep running Tor trainings. Please, [fill out this form](https://), byt the end of it, we will also ask your address to send to you a trainer kit (t-shirts and stickers).

We hope to hear back from you very soon! Have a great training.
