# Report - UR - TB Onboarding - Costa Rica

This is a report of an [exploratory, unmoderated test, that was run remotely](https://gitlab.torproject.org/tpo/ux/research/-/issues/29). The goal of the study was to understand the main problems users would encounter while trying to: install, configure and use Tor Browser. But also, to explore their impressions toward TB and compare them to their experience with other browsers. 

In summary: a total of 8 participants answered a survey consisting of 10 questions (7 of them were open-ended and 3 closed). One participant didn't end the survey and their answers were discarded for this analysis.

Author: [@josernitos](https://gitlab.torproject.org/josernitos) - Funded by: Internews - Feedback Collection Funding Pool 2021

## Main takeaways from study

##### Participants were confused to the value that Tor Browser provides them. 

For example, in asking how TB is compared to other browsers they mainly focused on the look and feel, to the point that most of them thought that TB was very similar to the browsers they normally used. And some passed the initial configuration not knowing the reason why there is one in the first place.

##### Most of the participants were not heavy technical users.

Most of them used MacOS and were able to install TB with ease (eventhough the user has to manually change their Security & Settings and accept opening the app). Some used interchangeably the words "buscador" which means *search engine* and "navegador" which means *browser*. And finally, most of them used Chrome as their day to day browser and only a few used Firefox. 

---

I wanted to **point out these patterns as they clearly emerged throughout the study**. I believe we should keep researching them as they touch on crucial design problems. Pursuing (or not) these questions would force us to consider the risk of building a browser that people choose and use for security concerns but are failing to use it in a way that would secure them. For example:

- Are we succesfully communicating the value of Tor Browser to users?
	- Does the design and interactions clearly suggest that (compared to other browsers) they are accessing the Internet in a completely different way?
- Are they aware of the limitations in protecting their privacy with Tor Browser? 
	- Do they understand why they can create a New Identity? 
	- Or why some sites might look differently using TB? And what they can do about it?
	- Or why is not recommended to install plugins or change the language TB would ask sites (other than english)?

## Overview of the tasks given to participants

| #    | Task                                                         |
| ---- | ------------------------------------------------------------ |
| a.1  | Be able to find Tor Browser.                                 |
| a.2  | Be able to install it correctly.                             |
| b.1  | Be able to configure TB correctly.                           |
| b.2  | (Open) Understand why it is necessary to configure it.       |
| c.1  | (Open) Understand if they see any problems accessing sites they normally use. |
| c.2  | (Open) Understand how different this experience is to their normal use of the internet (other browsers). |
| c.3  | (Open) Understand at first glance if they would use TB in their day to day, and why. |

### Finding, installing and configuring Tor Browser correctly (a, b):

All participants* were able to complete these tasks. Although there were doubts during the process.

**Finding Tor Browser** was confusing to some as they had to confirm they were on the correct search result. For example a participant just assumed the first search result was the correct one, another confirmed a visual reference as they had seen the logo before.

- This could be explained by the download Page Title: **"Download - Tor Project"** which appears in the search results and which does not contain the word *"Browser"*.
- It could also be that in the Download Page, there is no mention that in fact is a "Browser" download page. The first reference to the word Browser, is located in the screenshot of TB. But this screenshot is below the OS options, the language and other download options.

**Configuring TB** is only available in English if you downloaded from the default install option. A participant pointed out they were not able to change the language on the configuration and thus worried about people who wouldn't understand this step. Another participant didn't understand some words like Proxy.

**Only one participant pointed correctly why it is necessary to configure TB before running it**. The others didn't completely understand why is necessary this "extra step" or assumed something didn't applied to them or just clicked connect without reading the screens.

**Only one participant was not able to install Tor Browser correctly, until they restarted the machine. So, they answered everything except  b, c.1, c.2.*

### Using familiar sites with TB (c.1)

Three participants felt they had the same experience as usual, although one said they had to accept cookies for all the sites and that confused them. One participant experienced increased loading times but wasn't sure if it was their Internet connection or the TB. Two participants either couldn't access at all a familiar site; or were prompt with a captcha or a *NoScript XSS* warning appeared which confused them; or *cloudflare* identified them as a threat and were not able to access the site.

### Comparing their experience of TB with other browsers (c.2)

**Most of the users thought their experience were similar.** They mainly focused on how the browser looks and how it made them feel. Either 'not a lot of difference' or some difference: For example, three participants said that it looks very similar to other browsers, one participant pointed out that it had less colors and it was visually less attractive, and another one thought that it looked similar to Google.

### Using TB on their day to day (c.3)

**Only one participant said they would not use it** and pointed out that it looked like an 'old Firefox browser'. Another participant mentioned they would not use it in *some* ocassions as they have to connect to the Tor network and it was a tedious process. 

**The rest of them said they would use it.** Although some of them had doubts. For example if it will help them protect their privacy and data, or if it made them feel they are preventing risking any potential exposure when they access the Internet. A participant pointed that they would use it because there were less ads. And finally, one participant said that normally new things overwhelm them but they are willing to try. 

## Quotes

#### a, b

- "I went with the first option that appeared, assuming is the correct one"
- "Everything went smoothly. Although I know how to install it in my computer and is easy (mac)."
- "Since its not in the app store, I had to modify the Security settings to allow the installation"
- "... If you don't know english, then you can't use it. The accesibility is not taken as a default consideration."
- "I didn't managed to open the app. It's not clear what should I modify for it to work. It uses jargon as "proxy". And is only available in english."
- "I assume I'm free of the limitations they indicate, but I'm not sure exactly why"
- "The configuration is needed if your country has censorship"
- "I'm not sure, I didn't really see the step correctly. I just hit connect"

#### c.1

- "It felt similar as when I use other *search engines* that are familiar"
- "Some sites block the Tor connections. For example some government websites. Other sites ask for verifications that you are a living being and not a bot. Mostly those that protect against DDOS attacks."
- "In all of the sites they asked for cookies consent and those data that confuse me..."
- "It takes longer to load, I'm not sure if it's my connection or the browser"
- "A pop up appeared about an error that confused me (NoScript XSS Warning)"

#### c.2

- "There is not too much difference until now"
- "Is very similar"
- "It's similar, visually, to Google." "It makes me feel comfortable, I even forgot that I was using another browser"
- "The look and feel is not different, even though it works different internally"
- "It's different in some aspects"
- "It has less colors and is visually less attractive." "I feel like I'm doing extra work using the browser"

#### c.3

- "It looks like and old Firefox"
- "I'm not sure. Everything that's new overwhelms me, but I can try it."
- "If it's something that would help me regulate and protect my data, then its something that is worth using." "I always prefer to prevent than to feel that I'm exposed."
- "It's similar to other browsers I use."
- "I feel there are less ads."
- "When you choose TB as default browser it doesn't open links because they have to connect to the Tor network, so in the first try it will show an error, unless I was connected to it, and is a tedious process."
